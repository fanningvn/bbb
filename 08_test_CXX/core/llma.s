/**
 * @file llma.s
 * @author Alexis Marquet
 * @date 27 Feb 2015
 * @brief contains Low Level Memory Access functions
 **/

.globl PUT32
PUT32:
	str r1,[r0]
	bx lr
	
.globl GET32
GET32:
	ldr r0,[r0]
	bx lr
	
.globl PUT16
PUT16:
	strh r1,[r0]
	bx lr
	
.globl GET16
GET16:
	ldrh r0,[r0]
	bx lr
	
.globl PUT8
PUT8:
	strb r1,[r0]
	bx lr
	
.globl GET8
GET8:
	ldrb r0,[r0]
	bx lr

.globl BRANCHTO
BRANCHTO:
   mov pc, r0
   bx lr	/* never happens */


.globl CPU_irqE
CPU_irqE:
   dsb
	mrs r0, cpsr
	bic r0, r0, #0x80
	msr cpsr_c, r0
   DSB
   ISB
   bx lr

.globl CPU_irqD
CPU_irqD:
   dsb
   mrs r0, cpsr
   orr r0, r0, #0x80
   msr cpsr_c, r0
   DSB
   ISB
   bx lr

.syntax unified
.data
.global SOC_UART_0_REGS
SOC_UART_0_REGS             = 0x44E09000
/*
SOC_UART_1_REGS             = 0x48022000
SOC_UART_2_REGS             = 0x48024000
SOC_CM_PER_REGS             = 0x44E00000
CM_PER_L4LS_CLKSTCTRL       = 0x0
CM_PER_L4LS_CLKCTRL         = 0x60
CM_PER_UART1_CLKCTRL        = 0x6C
CM_PER_UART2_CLKCTRL        = 0x70
SOC_CM_WKUP_REGS            = 0x44E00400
CM_WKUP_CLKSTCTRL           = 0x0
CM_WKUP_UART0_CLKCTRL       = 0xB4
*/

SOC_CONTROL_REGS        = 0x44E10000
CONF_UART_0_RXD         = 0x970
CONF_UART_0_TXD         = 0x974
/*
CONF_UART_1_RXD         = 0x980
CONF_UART_1_TXD         = 0x984
CONF_UART_2_RXD         = 0x990
CONF_UART_2_TXD         = 0x994  // pattern continues
*/

UART_DLL           = 0x0
UART_RHR           = 0x0
UART_THR           = 0x0
UART_DLH           = 0x4
UART_IER           = 0x4
UART_FCR           = 0x8
UART_EFR           = 0x8
UART_IIR           = 0x8
UART_LCR           = 0xC
UART_MCR           = 0x10
UART_LSR           = 0x14
UART_MDR1          = 0x20
UART_SCR           = 0x40
UART_SYSC          = 0x54
UART_SYSS          = 0x58

.text
.globl UART0_INIT
UART0_INIT:
    r_base .req r0
    ldr r_base, =SOC_UART_0_REGS
    mov r1, #0x2                // SOFTRESET, spruh73l 19.5.1.43
    str r1, [r_base, #0x54]
1:  ldr r1, [r_base, #0x58]
    tst r1, #0x1                // wait for RESETDONE spruh73l tab 19-73
    beq 1b

    mov r1, #0x8               // disable IDLEMODE, spruh73l 19.5.1.43 & tab 19-72
    str r1, [r_base, #0x54]

    mov r1, #0x83          // DIV_EN (mode A), 8 data bits, spruh73l 19.5.1.13 19.4.1.1.2
    str r1, [r_base, #0x0C]
    mov r1, #0x1A          // CLOCK_LSB=0x1A, spruh73l 19.5.1.3
    str r1, [r_base, #0x00]

// the following code prepares FIFOs for IRQ
    mov r1, #0x10          // ENHANCEDEN=1 (enab R/W access to UART_FCR), spruh73l 19.5.1.8
    str r1, [r_base, #0x08]
    mov r1, #0x57          // FIFO triggers, clr & enab, spruh73l 19.5.1.11
    str r1, [r_base, #0x08]
    mov r1, #0x0           // ENHANCEDEN=0 (disab R/W access to UART_FCR), spruh73l 19.5.1.8
    str r1, [r_base, #0x08]
// end of FIFO-IRQ code

    mov r1, #0x0           // MODESELECT-UART 16x mode, spruh73l 19.5.1.26
    str r1, [r_base, #0x20]
    ldr r1, [r_base, #0x0C]
    bic r1, r1, #0x80      // clear DIV_EN, switch to operational mode, spruh73l 19.5.1.13
    str r1, [r_base, #0x0C]

// the following extra code prepares FIFOs for IRQ
    mov r1, #0xC8       // Rx & Tx FIFO granularity=1, TXEMPTYCTLIT=1 , spruh73l 19.5.1.39
    str r1, [r_base, #0x40]
// end of extra FIFO-IRQ code

    mov r1, #0x1                // enab interrupt RHR_IT, spruh73l 19.5.1.6, 19.3.6.2
    str r1, [r_base, #0x04]

    mov r0, #0x00
    bx lr

/*
 The MIT License (MIT)
 
 Copyright (c) 2015 Alexis Marquet
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
