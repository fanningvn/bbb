/**
 * @file llma.s
 * @author Alexis Marquet
 * @date 27 Feb 2015
 * @brief contains Low Level Memory Access functions
 **/

.globl PUT32
PUT32:
	str r1,[r0]
	bx lr
	
.globl GET32
GET32:
	ldr r0,[r0]
	bx lr
	
.globl PUT16
PUT16:
	strh r1,[r0]
	bx lr
	
.globl GET16
GET16:
	ldrh r0,[r0]
	bx lr
	
.globl PUT8
PUT8:
	strb r1,[r0]
	bx lr
	
.globl GET8
GET8:
	ldrb r0,[r0]
	bx lr

.globl BRANCHTO
BRANCHTO:
   mov pc, r0
   bx lr	/* never happens */


.globl CPU_irqE
CPU_irqE:
   dsb
	mrs r0, cpsr
	bic r0, r0, #0x80
	msr cpsr_c, r0
   DSB
   ISB
   bx lr

.globl CPU_irqD
CPU_irqD:
   dsb
   mrs r0, cpsr
   orr r0, r0, #0x80
   msr cpsr_c, r0
   DSB
   ISB
   bx lr

.syntax unified
.data
.global SOC_UART_0_REGS
SOC_UART_0_REGS             = 0x44E09000
/*
SOC_UART_1_REGS             = 0x48022000
SOC_UART_2_REGS             = 0x48024000
SOC_CM_PER_REGS             = 0x44E00000
CM_PER_L4LS_CLKSTCTRL       = 0x0
CM_PER_L4LS_CLKCTRL         = 0x60
CM_PER_UART1_CLKCTRL        = 0x6C
CM_PER_UART2_CLKCTRL        = 0x70
SOC_CM_WKUP_REGS            = 0x44E00400
CM_WKUP_CLKSTCTRL           = 0x0
CM_WKUP_UART0_CLKCTRL       = 0xB4
*/

SOC_CONTROL_REGS        = 0x44E10000
CONF_UART_0_RXD         = 0x970
CONF_UART_0_TXD         = 0x974
/*
CONF_UART_1_RXD         = 0x980
CONF_UART_1_TXD         = 0x984
CONF_UART_2_RXD         = 0x990
CONF_UART_2_TXD         = 0x994  // pattern continues
*/

UART_DLL           = 0x0
UART_RHR           = 0x0
UART_THR           = 0x0
UART_DLH           = 0x4
UART_IER           = 0x4
UART_FCR           = 0x8
UART_EFR           = 0x8
UART_IIR           = 0x8
UART_LCR           = 0xC
UART_MCR           = 0x10
UART_LSR           = 0x14
UART_MDR1          = 0x20
UART_SCR           = 0x40
UART_SYSC          = 0x54
UART_SYSS          = 0x58

.text
.globl UART0_INIT
UART0_INIT:
    r_base .req r0
    ldr r_base, =SOC_UART_0_REGS
    mov r1, #0x2                // SOFTRESET, spruh73l 19.5.1.43
    str r1, [r_base, #0x54]
1:  ldr r1, [r_base, #0x58]
    tst r1, #0x1                // wait for RESETDONE spruh73l tab 19-73
    beq 1b

    mov r1, #0x8               // disable IDLEMODE, spruh73l 19.5.1.43 & tab 19-72
    str r1, [r_base, #0x54]

    mov r1, #0x83          // DIV_EN (mode A), 8 data bits, spruh73l 19.5.1.13 19.4.1.1.2
    str r1, [r_base, #0x0C]
    mov r1, #0x1A          // CLOCK_LSB=0x1A, spruh73l 19.5.1.3
    str r1, [r_base, #0x00]

// the following code prepares FIFOs for IRQ
    mov r1, #0x10          // ENHANCEDEN=1 (enab R/W access to UART_FCR), spruh73l 19.5.1.8
    str r1, [r_base, #0x08]
    mov r1, #0x57          // FIFO triggers, clr & enab, spruh73l 19.5.1.11
    str r1, [r_base, #0x08]
    mov r1, #0x0           // ENHANCEDEN=0 (disab R/W access to UART_FCR), spruh73l 19.5.1.8
    str r1, [r_base, #0x08]
// end of FIFO-IRQ code

    mov r1, #0x0           // MODESELECT-UART 16x mode, spruh73l 19.5.1.26
    str r1, [r_base, #0x20]
    ldr r1, [r_base, #0x0C]
    bic r1, r1, #0x80      // clear DIV_EN, switch to operational mode, spruh73l 19.5.1.13
    str r1, [r_base, #0x0C]

// the following extra code prepares FIFOs for IRQ
    mov r1, #0xC8       // Rx & Tx FIFO granularity=1, TXEMPTYCTLIT=1 , spruh73l 19.5.1.39
    str r1, [r_base, #0x40]
// end of extra FIFO-IRQ code

    mov r1, #0x1                // enab interrupt RHR_IT, spruh73l 19.5.1.6, 19.3.6.2
    str r1, [r_base, #0x04]

    mov r0, #0x00
    bx lr

















//
// interrupt service routines
//
.syntax unified
.data
SOC_AINTC_REGS          = 0x48200000  // BBB ARM Interrupt Controller base address
INTC_SYSCONFIG          = 0x10
INTC_SYSSTATUS          = 0x14
INTC_SIR_IRQ            = 0x40
INTC_CONTROL            = 0x48
INTC_MIR_CLEAR1         = 0xA8
INTC_MIR_CLEAR2         = 0xC8

.text
//
// ARM interrupt controller init
//
.global irq_init
irq_init:
    r_base .req r0
    ldr r_base, =SOC_AINTC_REGS
    mov r1, 0x2
    str r1, [r_base, INTC_SYSCONFIG]   // soft reset AINT controller spruh73l 6.5.1.2
1:  ldr	r1, [r_base, INTC_SYSSTATUS]   // (not really necessary)
    and r1, r1, 0x1
    cmp	r1, 0x1                        // test for reset complete spruh73l 6.5.1.3
    bne	1b

    mov r1, 0x1            // unmask interrupts on peripheral side spruh73l 6.2.1 & 6.3
    str r1, [r_base, INTC_MIR_CLEAR1]    // INTC_MIR_CLEAR1 #32 GPIOINT2A (for GPIOIRQ0)
    mov r1, 0x1 << 8
    str r1, [r_base, INTC_MIR_CLEAR2]    // INTC_MIR_CLEAR2 #72 UART0INT
    mov r1, 0x1 << 11
    str r1, [r_base, INTC_MIR_CLEAR2]    // INTC_MIR_CLEAR2 #75 RTCINT

// spruh73l 26.1.3.2 default boot procedure uses address 0x4030CE00
// for base of RAM exception vectors.  see tab 26.3 for vector addresses
    ldr r_base, =0x4030CE24   // register UND in interrupt vector
    ldr r1, =und_isr
    str r1, [r_base]
    ldr r_base, =0x4030CE38   // register IRQ in interrupt vector
    ldr r1, =irq_isr
    str r1, [r_base]

    mrs r1, cpsr
    bic r1, r1, #0x80  // enable IRQ, ie unmask IRQ bit of cpsr
    msr cpsr_c, r1     // 9.2.3.1 & fig 2.3 ARM System Developer’s Guide, Sloss et al

    bx lr
    .unreq r_base

//
// UND  undefined interrupt service routine
//
.global und_isr
und_isr:
    stmfd sp!, {r0-r1, r12, lr} // must align stack to 8-byte boundary, AAPCS
    mrs r12, spsr               // save program status register

    ldr r0, =SOC_UART_0_REGS  // print text "UND"
    mov r1, 0x55
    bl uart_tx
    mov r1, 0x4E
    bl uart_tx
    mov r1, 0x44
    bl uart_tx
    mov r1, 0x0A
    bl uart_tx

    msr spsr, r12                // restore status
    ldmfd sp!, {r0-r1, r12, pc}^ // ^ cpsr restored from spsr. do not adjust lr with UND
				 // cuz the instruction is undefined so skip over it

//
// IRQ interrupt service routine
//
// hacked from Al Selen, github.com/auselen
// & Mattius van Duin, TI.com e2e forum
// Non-nested interrupt handler per Sloss, 9.3.1
// Designed for very lean ISRs that are simple, fast and no loops.
//
.global irq_isr
irq_isr:
    stmfd sp!, {r0-r3, r12, lr} // must align stack to 8-byte boundary
    mrs r12, spsr               // save program status register

    ldr r0, =SOC_AINTC_REGS
    ldr r1, [r0, INTC_SIR_IRQ]  // fetch SIR_IRQ register spruh73l 6.2.2 & 6.5.1.4

    cmp r1, 0x80                // check spurious irq (only bits 0-6 are valid)
    bge irq_isr_exit            // still getting spurious irqs

    adr r2, irq_vector          // jump to specific irq isr
    ldr r2, [r2, r1, lsl 2]
    blx r2

irq_isr_exit:
    ldr r0, =SOC_AINTC_REGS
    ldr r1, =0x1                // NewIRQAgr bit, reset IRQ output and enable new IRQ
    str r1, [r0, INTC_CONTROL]  // spruh73l 6.2.2 & 6.5.1.6

    msr spsr, r12               // restore status
    ldmfd sp!, {r0-r3, r12, lr}
    subs pc, lr, #4             // adjust lr for pipeline & return to normal execution

//
//  ARM Cortex-A8 Interrupts  spruh73l 6.3
//
irq_vector:
.word	0            // 0   Cortex-A8 ICECrusher
.word	0            // 1   Cortex-A8 debug tx
.word	0            // 2   Cortex-A8 debug rx
.word	0            // 3   Cortex-A8 PMU
.word	0            // 4   ELM
.word	0            // 5   SSM WFI
.word	0            // 6   SSM
.word	0            // 7   External IRQ ("NMI")
.word	0            // 8   L3 firewall error
.word	0            // 9   L3 interconnect debug error
.word	0            // 10  L3 interconnect non-debug error
.word	0            // 11  PRCM MPU irq
.word	0            // 12  EDMA client 0
.word	0            // 13  EDMA protection error
.word	0            // 14  EDMA CC error
.word	0            // 15  Watchdog 0
.word	0            // 16  ADC / Touchscreen controller
.word	0            // 17  USB queue manager and CPPI
.word	0            // 18  USB port 0
.word	0            // 19  USB port 1
.word	0            // 20  PRUSS host event 0
.word	0            // 21  PRUSS host event 1
.word	0            // 22  PRUSS host event 2
.word	0            // 23  PRUSS host event 3
.word	0            // 24  PRUSS host event 4
.word	0            // 25  PRUSS host event 5
.word	0            // 26  PRUSS host event 6
.word	0            // 27  PRUSS host event 7
.word	0            // 28  MMC/SD 1
.word	0            // 29  MMC/SD 2
.word	0            // 30  I2C 2
.word	0            // 31  eCAP 0
.word	gpio_isr     // 32  GPIO 2 irq 0  -  GPIOINT2A
.word	0            // 33  GPIO 2 irq 1
.word	0            // 34  USB wakeup
.word	0            // 35  PCIe wakeup
.word	0            // 36  LCD controller
.word	0            // 37  SGX530 error in IMG bus
.word	0            // 38  reserved
.word	0            // 39  ePWM 2
.word	0            // 40  Ethernet core 0 rx low on bufs
.word	0            // 41  Ethernet core 0 rx dma completion
.word	0            // 42  Ethernet core 0 tx dma completion
.word	0            // 43  Ethernet core 0 misc irq
.word	0            // 44  UART 3
.word	0            // 45  UART 4
.word	0            // 46  UART 5
.word	0            // 47  eCAP 1
.word	0            // 48  reserved
.word	0            // 49  reserved
.word	0            // 50  reserved
.word	0            // 51  reserved
.word	0            // 52  DCAN 0 irq 0
.word	0            // 53  DCAN 0 irq 1
.word	0            // 54  DCAN 0 parity
.word	0            // 55  DCAN 1 irq 0
.word	0            // 56  DCAN 1 irq 1
.word	0            // 57  DCAN 1 parity
.word	0            // 58  ePWM 0 TZ
.word	0            // 59  ePWM 1 TZ
.word	0            // 60  ePWM 2 TZ
.word	0            // 61  eCAP 2
.word	0            // 62  GPIO 3 irq 0
.word	0            // 63  GPIO 3 irq 1
.word	0            // 64  MMC/SD 0
.word	0            // 65  SPI 0
.word	0            // 66  Timer 0
.word	0            // 67  Timer 1
.word	0            // 68  Timer 2
.word	0            // 69  Timer 3
.word	0            // 70  I2C 0
.word	0            // 71  I2C 1
.word	uart0_isr    // 72  UART 0  -  UART0INT
.word	0            // 73  UART 1
.word	0            // 74  UART 2
.word	rtc_isr      // 75  RTC periodic  -  RTCINT
.word	0            // 76  RTC alarm
.word	0            // 77  System mailbox irq 0
.word	0            // 78  Wakeup-M3
.word	0            // 79  eQEP 0
.word	0            // 80  McASP 0 out
.word	0            // 81  McASP 0 in
.word	0            // 82  McASP 1 out
.word	0            // 83  McASP 1 in
.word	0            // 84  reserved
.word	0            // 85  reserved
.word	0            // 86  ePWM 0
.word	0            // 87  ePWM 1
.word	0            // 88  eQEP 1
.word	0            // 89  eQEP 2
.word	0            // 90  External DMA/IRQ pin 2
.word	0            // 91  Watchdog 1
.word	0            // 92  Timer 4
.word	0            // 93  Timer 5
.word	0            // 94  Timer 6
.word	0            // 95  Timer 7
.word	0            // 96  GPIO 0 irq 0
.word	0            // 97  GPIO 0 irq 1
.word	0            // 98  GPIO 1 irq 0
.word	0            // 99  GPIO 1 irq 1
.word	0            // 100 GPMC
.word	0            // 101 EMIF 0 error
.word	0            // 102 reserved
.word	0            // 103 reserved
.word	0            // 104 reserved
.word	0            // 105 reserved
.word	0            // 106 reserved
.word	0            // 107 reserved
.word	0            // 108 reserved
.word	0            // 109 reserved
.word	0            // 110 reserved
.word	0            // 111 reserved
.word	0            // 112 EDMA TC 0 error
.word	0            // 113 EDMA TC 1 error
.word	0            // 114 EDMA TC 2 error
.word	0            // 115 Touchscreen Pen
.word	0            // 116 reserved
.word	0            // 117 reserved
.word	0            // 118 reserved
.word	0            // 119 reserved
.word	0            // 120 SmartReflex 0 (MPU)
.word	0            // 121 SmartReflex 1 (core)
.word	0            // 122 reserved
.word	0            // 123 External DMA/IRQ pin 0
.word	0            // 124 External DMA/IRQ pin 1
.word	0            // 125 SPI 1
.word	0            // 126 reserved
.word	0            // 127 reserved















//
// UART routines
//
.syntax unified
.data
.global SOC_UART_0_REGS
SOC_UART_0_REGS             = 0x44E09000
/*
SOC_UART_1_REGS             = 0x48022000
SOC_UART_2_REGS             = 0x48024000
SOC_CM_PER_REGS             = 0x44E00000
CM_PER_L4LS_CLKSTCTRL       = 0x0
CM_PER_L4LS_CLKCTRL         = 0x60
CM_PER_UART1_CLKCTRL        = 0x6C
CM_PER_UART2_CLKCTRL        = 0x70
SOC_CM_WKUP_REGS            = 0x44E00400
CM_WKUP_CLKSTCTRL           = 0x0
CM_WKUP_UART0_CLKCTRL       = 0xB4
*/

SOC_CONTROL_REGS        = 0x44E10000
CONF_UART_0_RXD         = 0x970
CONF_UART_0_TXD         = 0x974
/*
CONF_UART_1_RXD         = 0x980
CONF_UART_1_TXD         = 0x984
CONF_UART_2_RXD         = 0x990
CONF_UART_2_TXD         = 0x994  // pattern continues
*/

UART_DLL           = 0x0
UART_RHR           = 0x0
UART_THR           = 0x0
UART_DLH           = 0x4
UART_IER           = 0x4
UART_FCR           = 0x8
UART_EFR           = 0x8
UART_IIR           = 0x8
UART_LCR           = 0xC
UART_MCR           = 0x10
UART_LSR           = 0x14
UART_MDR1          = 0x20
UART_SCR           = 0x40
UART_SYSC          = 0x54
UART_SYSS          = 0x58

.text
//
// uart0 module init
//
// @return   0=success or 1=fail
//
.global uart0_init
uart0_init:
    r_base .req r0
    ldr r_base, =SOC_UART_0_REGS
    mov r1, 0x2                // SOFTRESET, spruh73l 19.5.1.43
    str r1, [r_base, UART_SYSC]
1:  ldr r1, [r_base, UART_SYSS]
    tst r1, 0x1                // wait for RESETDONE spruh73l tab 19-73
    beq 1b

    mov r1, #0x8               // disable IDLEMODE, spruh73l 19.5.1.43 & tab 19-72
    str r1, [r_base, UART_SYSC]

    mov r1, 0x83          // DIV_EN (mode A), 8 data bits, spruh73l 19.5.1.13 19.4.1.1.2
    str r1, [r_base, UART_LCR]
    mov r1, 0x1A          // CLOCK_LSB=0x1A, spruh73l 19.5.1.3
    str r1, [r_base, UART_DLL]

// the following code prepares FIFOs for IRQ
    mov r1, 0x10          // ENHANCEDEN=1 (enab R/W access to UART_FCR), spruh73l 19.5.1.8
    str r1, [r_base, UART_EFR]
    mov r1, 0x57          // FIFO triggers, clr & enab, spruh73l 19.5.1.11
    str r1, [r_base, UART_FCR]
    mov r1, 0x0           // ENHANCEDEN=0 (disab R/W access to UART_FCR), spruh73l 19.5.1.8
    str r1, [r_base, UART_EFR]
// end of FIFO-IRQ code

    mov r1, 0x0           // MODESELECT-UART 16x mode, spruh73l 19.5.1.26
    str r1, [r_base, UART_MDR1]
    ldr r1, [r_base, UART_LCR]
    bic r1, r1, 0x80      // clear DIV_EN, switch to operational mode, spruh73l 19.5.1.13
    str r1, [r_base, UART_LCR]

// the following extra code prepares FIFOs for IRQ
    mov r1, 0xC8       // Rx & Tx FIFO granularity=1, TXEMPTYCTLIT=1 , spruh73l 19.5.1.39
    str r1, [r_base, UART_SCR]
// end of extra FIFO-IRQ code

    mov r1, 0x1                // enab interrupt RHR_IT, spruh73l 19.5.1.6, 19.3.6.2
    str r1, [r_base, UART_IER]

    mov r0, 0
    bx lr
    .unreq r_base

//
// uart0 interrupt service routine
//
// see interrupt handling procedure spruh73l 19.3.5.1.1
//
// RHR interrupt is enabled most the time.  It is momentarily
// disabled by this ISR during processing, then re-enabled by this ISR.
//
// THR interrupt is disabled most of the time.  It is momentarily
// enabled by uart_txi() to transmit a byte, then disabled by this ISR.
//
.global uart0_isr
uart0_isr:
    ldr r0, =SOC_UART_0_REGS
    ldr r1, [r0, UART_IIR]      // read interrupt ID register spruh73l 19.5.1.9
    mov r2, 0x0                 // disab UART interrupts (clobber them all)
    str r2, [r0, UART_IER]
    and r1, r1, #0x3E           // strip out IT_TYPE  tab 19-38
    cmp r1, #0x4                // RHR interrupt bit
    bne 1f
    ldr r2, [r0, UART_RHR]      // read byte
    ldr r1, =uart0_rbuf         // store byte in C variable
    str r2, [r1]
    b uart0_isr_exit
1:
    cmp r1, #0x2                // THR interrupt bit
    bne uart0_isr_exit
    ldr r1, =uart0_tbuf         // get tx byte
    ldr r2, [r1]
    str r2, [r0, UART_THR]      // write out byte

uart0_isr_exit:
    ldr r2, =uart0_irq_count    // increment counter, C variable
    ldr r1, [r2]
    add r1, r1, #0x1
    str r1, [r2]

    mov r2, 0x1                 // re-enab RHR interrupts only
    str r2, [r0, UART_IER]
    bx lr

//
// transmit a single byte with UARTx (interrupt mode)
//
// @param uart_base_addr    uint32, UARTx module base address
//
// The transmit buffer uart0_tbuf (a global variable) must contain
// the data in the LSByte before calling this routine.
//
.global uart_txi
uart_txi:
    r_base .req r0
    mov r1, 0x3                // enab interrupts: THR_IT RHR_IT, spruh73l 19.5.1.6, 19.3.6.2
    str r1, [r_base, UART_IER] // this will cause a THR interrupt pronto
    bx lr
    .unreq r_base

//
// receive a single byte from UARTx (poll mode)
//
// @param uart_base_addr    uint32, UARTx module base address
//
// @return                  uint32, LSByte contains a single received byte
//
.global uart_rx
uart_rx:
    r_base .req r0
    r_byte .req r1
1:  ldr	r2, [r_base, UART_LSR]
    tst	r2, 0x1               // wait for rx FIFO not empty, spruh73l tab 19-48
    beq	1b
    ldr r_byte, [r_base, 0x0]
    mov r0, r_byte
    bx lr
    .unreq r_base
    .unreq r_byte

//
// transmit a single byte with UARTx (poll mode)
//
// @param uart_base_addr    uint32, UARTx module base address
// @param uart_byte         uint32, LSByte contains a single byte to transmit
//
.global uart_tx
uart_tx:
    r_base .req r0
    r_byte .req r1
1:  ldr	r2, [r_base, UART_LSR]
    tst	r2, 0x20             // wait for tx FIFO empty, spruh73l 19.5.1.19, tab 19-48
    beq	1b
    str r_byte, [r_base, 0x0]
    bx lr
    .unreq r_base
    .unreq r_byte

//
// pinmux setup
//
// @param pin          uint32, pinmux address (offset = 0x800–0xA34) spruh73l 9.3.1.49
// @param val          uint32, pinmux mode/value spruh73l 9.2.2
//
// eg  pin:  CONF_UART_1_RXD (=0x980) for UART1 Rx pin, see hw_control_AM335x.h
//     pin:  GPIO_1_23 (=0x85C) for LED USR3, see pin_mux.h
//     val:  mov r_tmp, #0x27    Fast slew, Rx disab, pull-up enab, mode 7
//     values/default values are pad dependant, see spruh73l 9.2.2 or Peripheral doc
//     set mode per P8 & P9 Header Tables, see www.derekmolloy.ie
//
.global pinmux
pinmux:
    r_base .req r0
    r_pin .req r1
    r_val .req r2
    ldr r_base, =SOC_CONTROL_REGS
    str r_val, [r_base, r_pin]
    bx lr
    .unreq r_base
    .unreq r_pin
    .unreq r_val
//
// hexprint
//
// @param data              uint32, word (4 bytes) to print in hex format
//
.global hexprint
hexprint:
    r_data .req r0
    r_base .req r1
    stmfd sp!, {r0-r4, lr}
    mov r4, r_data
    mov r3, #0x8
    ldr r_base, =SOC_UART_0_REGS
hexloop:
    mov r4, r4, ror #28
    and r_data, r4, #0xF
    cmp r_data, #0xA
    addlt r_data, r_data, #0x30
    addge r_data, r_data, #0x37
1:  ldr	r2, [r_base, UART_LSR]
    and r2, r2, 0x20
    cmp	r2, 0x20             // wait for tx FIFO empty, spruh73l tab 19-48
    bne	1b
    str r_data, [r_base, 0x0]
    sub r3, r3, #0x1
    cmp r3, #0x0
    bne hexloop
    mov r_data, 0x0A
1:  ldr	r2, [r_base, UART_LSR]
    and r2, r2, 0x20
    cmp	r2, 0x20             // wait for tx FIFO empty, spruh73l tab 19-48
    bne	1b
    str r_data, [r_base, 0x0]
    ldmfd sp!, {r0-r4, pc}
    .unreq r_data
    .unreq r_base



















//
// GPIO routines
//
.syntax unified
.data
SOC_CM_PER_REGS         = 0x44E00000
CM_PER_GPIO1_CLKCTRL    = 0xac
CM_PER_GPIO2_CLKCTRL    = 0xb0

SOC_CONTROL_REGS        = 0x44E10000
GPIO_1_21               = 0x854      // LED USR1
GPIO_1_22               = 0x858      // LED USR2
GPIO_1_23               = 0x85C      // LED USR3
GPIO_1_24               = 0x860      // LED USR4

.global SOC_GPIO_1_REGS    // allow global access to USR LEDs
.global GPIO_CLEARDATAOUT
.global GPIO_SETDATAOUT

SOC_GPIO_0_REGS         = 0x44E07000
SOC_GPIO_1_REGS         = 0x4804c000
SOC_GPIO_2_REGS         = 0x481ac000
SOC_GPIO_3_REGS         = 0x481AE000
GPIO_SYSCONFIG          = 0x10
GPIO_IRQSTATUS_RAW_0    = 0x24
GPIO_IRQSTATUS_0 	      = 0x2c
GPIO_IRQSTATUS_SET_0    = 0x34
GPIO_SYSSTATUS          = 0x114
GPIO_CTRL               = 0x130
GPIO_OE                 = 0x134
GPIO_DATAIN	            = 0x138
GPIO_LEVELDETECT0       = 0x140
GPIO_LEVELDETECT1       = 0x144
GPIO_RISINGDETECT	      = 0x148
GPIO_FALLINGDETECT      = 0x14c
GPIO_DEBOUNCENABLE	    = 0x150
GPIO_DEBOUNCINGTIME     = 0x154
GPIO_CLEARDATAOUT		    = 0x190
GPIO_SETDATAOUT		      = 0x194

.text
//
// gpio module init
// hacked from Nick Kondrashov github/spbnick
// & Mattius van Duin, TI e2e forum
//
// @param gpio_base_addr      uint32, GPIO module base address
// @param gpio_pins           uint32, a bit for each pin
//
// @return   0=success or 1=fail
//
.global gpio_init
gpio_init:
    r_base .req r0
    r_pin .req r1
    ldr r2, =SOC_CM_PER_REGS            // Enable GPIO module clocks
    ldr r3, =0x40002                    // FCLK_EN, MODULEMODE ENABLE, spruh73l 8.1.12.1.30
    str r3, [r2, CM_PER_GPIO1_CLKCTRL]  // for USRLEDs
    str r3, [r2, CM_PER_GPIO2_CLKCTRL]  // for boot button GPIO2_8

    mvn r_pin, r_pin                // enable pins/lines for output, spruh73l 25.4.1.16
    str r_pin, [r_base, GPIO_OE]

    ldr	r2, =SOC_GPIO_2_REGS
    mov	r3, 0x2                     // soft reset gpio module, spruh73l 25.3.2.4, tab 25-7
    str	r3, [r2, GPIO_SYSCONFIG]
1:  ldr	r3, [r2, GPIO_SYSSTATUS]
    tst	r3, 0x1                     // wait for reset complete, spruh73l tab 25-19
    beq	1b
    mov r3, 0x10                    // IDLEMODE = smart
    str	r3, [r2, GPIO_SYSCONFIG]
    mov	r3, 0x0
    str	r3, [r2, GPIO_CTRL]         // module enable (default)
    mov	r3, 1 << 8                  // setup boot button for irq
    str	r3, [r2, GPIO_OE]           // enable for input (default)
    str	r3, [r2, GPIO_IRQSTATUS_SET_0]
    str	r3, [r2, GPIO_DEBOUNCENABLE]
    str	r3, [r2, GPIO_RISINGDETECT]
    str	r3, [r2, GPIO_FALLINGDETECT]
    mov r3, 0x10                      // debounce time n * 31usec
    str	r3, [r2, GPIO_DEBOUNCINGTIME]
    mov r_base, #0x0
    bx lr
    .unreq r_base
    .unreq r_pin

//
// gpio interrupt service routine
//
// hacked from Mattius van Duin, TI e2e forum
//
.global gpio_isr
gpio_isr:
    ldr	r0, =SOC_GPIO_2_REGS        // boot button is at GPIO2_8
    mov	r1, 1 << 8                  // clear INTLINE[8] of GPIOIRQ0 interrupt
    str	r1, [r0, GPIO_IRQSTATUS_0]  // spruh73l 25.4.1.6

    ldr	r1, [r0, GPIO_DATAIN]       // read all GPIO2 pins
    tst	r1, 1 << 8                  // test boot button
    ldr	r0, =SOC_GPIO_1_REGS        // actuate USRLED2
    mov	r1, 1 << 22                 // USRLED2 is GPIO1_22
    beq	1f
    str	r1, [r0, GPIO_CLEARDATAOUT]
    b gpio_isr_exit
1:  str	r1, [r0, GPIO_SETDATAOUT]

gpio_isr_exit:
    ldr r0, =gpio_irq_count    // increment counter, C variable
    ldr r1, [r0]
    add r1, r1, #0x1
    str r1, [r0]
    bx lr

//
// turn on/set gpio pin/line(s)
//
// @param gpio_base_addr    uint32, GPIO module base address
// @param gpio_pins         uint32, a bit for every pin/line
//
.global gpio_on
gpio_on:
    r_base .req r0
    r_pin .req r1
    str r_pin, [r_base, GPIO_SETDATAOUT]
    bx lr
    .unreq r_base
    .unreq r_pin

//
// turn off/reset gpio pin/line(s)
//
// @param gpio_base_addr    uint32, GPIO module base address
// @param gpio_pins         uint32, a bit for every pin/line
//
.global gpio_off
gpio_off:
    r_base .req r0
    r_pin .req r1
    str r_pin, [r_base, GPIO_CLEARDATAOUT]
    bx lr
    .unreq r_base
    .unreq r_pin

//
// read gpio pins/lines
//
// @param gpio_base_addr    uint32, GPIO module base address
//
// @return   r0 - uint32, sampled input data, a bit for every pin/line
//
.global gpio_read
gpio_read:
    r_base .req r0
    ldr	r1, [r0, GPIO_DATAIN]       // read all 32 GPIO pins
    mov r0, r1
    bx lr
    .unreq r_base
















// Timer routines: RTC
//
.syntax unified
.data
CM_PER_BASE              = 0x44E00000
CM_RTC_RTC_CLKCTRL       = 0x0  // spruh73l 8.1.12.6.1
CM_RTC_CLKSTCTRL         = 0x4  // spruh73l 8.1.12.6.2

SOC_RTC_0_REGS           = 0x44E3E000
RTC_CTRL_REG             = 0x40
RTC_STATUS_REG           = 0x44
RTC_INTERRUPTS_REG       = 0x48
RTC_OSC_REG              = 0x54
KICK0R                   = 0x6C
KICK1R                   = 0x70
SECONDS_REG              = 0x00
MINUTES_REG              = 0x04
HOURS_REG                = 0x08
DAYS_REG                 = 0x0C
MONTHS_REG               = 0x10
YEARS_REG                = 0x14
WEEKS_REG                = 0x18

GPIO_CLEARDATAOUT        = 0x190
GPIO_SETDATAOUT          = 0x194

.text
//
// rtc module init
//
// @return   0=success or 1=fail
//
.global rtc_init
rtc_init:
    r_base .req r0
    ldr r_base, =CM_PER_BASE
    mov r1, 0x2
    str r1, [r_base, CM_RTC_RTC_CLKCTRL]  // MODULEMODE enab spruh73l 8.1.12.6.1
    mov r1, 0x2                           // transition crtl SW_ WKUP spruh73l 8.1.12.6.2
    str r1, [r_base, CM_RTC_CLKSTCTRL]

    ldr r_base, =SOC_RTC_0_REGS
    ldr r1, =0x83E70B13          // disab write protection
    str r1, [r_base, KICK0R]
    ldr r1, =0x95A4F1E0
    str r1, [r_base, KICK1R]
    ldr r1, =0x48                // select 32k ext clk & enab, spruh73l 20.3.3.2, tab 20.82
    str r1, [r_base, RTC_OSC_REG]
    ldr r1, =0x1                 // RTC enab, functional & running, spruh73l tab 20.77
    str r1, [r_base, RTC_CTRL_REG]

1:  ldr r1, [r_base, RTC_STATUS_REG]
    and r1, r1, 0x1
    cmp	r1, 0x0                        // wait for BUSY bit to clear, spruh73l 20.3.5.15
    bne	1b

    ldr r1, =0x4                       // enab interrupt every second, spruh73l 20.3.5.16
    str r1, [r_base, RTC_INTERRUPTS_REG]
    mov r_base, #0x0
    bx lr
    .unreq r_base

//
// rtc interrupt service routine
//
// hacked from Al Selen at github/auselen
//
.global rtc_isr
rtc_isr:
    ldr r0, =SOC_RTC_0_REGS   // retrieve time from RTC, spruh73l 20.3.3.5.1
    ldr r1, =sec              // C variable
    ldr r2, [r0, SECONDS_REG]
    str r2, [r1]
    ldr r1, =min              // C variable
    ldr r2, [r0, MINUTES_REG]
    str r2, [r1]
    ldr r1, =hour             // C variable
    ldr r2, [r0, HOURS_REG]
    str r2, [r1]
    ldr r1, =rtc_irq_count    // C variable, increment counter
    ldr r2, [r1]
    add r2, r2, #0x1
    str r2, [r1]

    ldr r0, =SOC_GPIO_1_REGS  // actuate LED USR1
    mov r1, 0x1<<21
    ands r2, r2, 0x1          // flip-flop on rtc_irq_count
    beq 1f
    str r1, [r0, GPIO_SETDATAOUT]
    bx lr
1:
    str r1, [r0, GPIO_CLEARDATAOUT]
    bx lr









/*
 The MIT License (MIT)
 
 Copyright (c) 2015 Alexis Marquet
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */
