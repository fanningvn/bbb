/**
 * @file main.c
 * @author Alexis Marquet
 * @date 29 Apr 2015
 * @brief contains the main application
 **/


/**
 * @mainpage
 * 08_test_CXX: in this one, I tried to get cxx linkage to work. proof with vector, string, and ostream.
 * first you should see the cout << output, then the led_class ctor, and the value "seconds" from the RTC should print.
 * and after 16 seconds, the dtor, and ctor again.
 * the executable size of this example is huge, i dont know why it is THAT huge.
 **/

extern "C" {

#include <stdio.h>

#include "../core/llma.h"
#include "../proc/clock_module.h"
#include "../proc/interrupt.h"
#include "../board/LED.h"
#include "../proc/UART.h"

extern uint32_t __data_end__	;
extern uint32_t __data_start__	;
volatile uint32_t rtc_irq_count=0;
volatile uint32_t hour, min, sec;
volatile uint32_t gpio_irq_count=0;
volatile uint32_t uart0_irq_count=0;
volatile uint32_t uart0_rbuf;
volatile uint32_t uart0_tbuf;
}

#include <vector>
#include <string>
#include <iostream>

using namespace std;

extern "C"
void UART0INT_IRQHandler() {

	asm volatile (
				"		ldr r0, =SOC_UART_0_REGS \n"
				"		ldr r1, [r0, #0x8]   \n"   // read interrupt ID register spruh73l 19.5.1.9
				"		mov r2, #0x0              \n"   // disab UART interrupts (clobber them all)
				"		str r2, [r0, #0x04]	 \n"
				"		and r1, r1, #0x3E        \n"   // strip out IT_TYPE  tab 19-38
				"		cmp r1, #0x4             \n"   // RHR interrupt bit
				"		bne 1f					 \n"
				"		ldr r2, [r0, #0x00]      \n"   // read byte
				"		ldr r1, =uart0_rbuf      \n"   // store byte in C variable
				"		str r2, [r1]			 \n"
				"		b uart0_isr_exit         \n"
				"	1:							 \n"
				"		cmp r1, #0x2             \n"   // THR interrupt bit
				"		bne uart0_isr_exit		 \n"
				"		ldr r1, =uart0_tbuf      \n"   // get tx byte
				"		ldr r2, [r1]			 \n"
				"		str r2, [r0, #0x00]      \n"   // write out byte

				"	uart0_isr_exit:				 \n"
				"		ldr r2, =uart0_irq_count \n"   // increment counter, C variable
				"		ldr r1, [r2]			 \n"
				"		add r1, r1, #0x1		 \n"
				"		str r1, [r2]             \n"

				"		mov r2, #0x1              \n"     // re-enab RHR interrupts only
				"		str r2, [r0, #0x04]   \n"
				"		bx lr "
				);
}

extern "C"
void WDT1INT_IRQHandler() {
	printf("Fucking watdog\r\n");
}

uint16_t count, wtg_X;

uint32_t SYN_WDT_LOAD_VALUE;

extern "C"
void RTCINT_IRQHandler() {
	wtg_X++;
	PUT32(0x44E35030, wtg_X);

	uint32_t WDT_FUCKING    = GET32(0x44E35000 + 0x14);

	uint32_t WDT_LOAD_VALUE = GET32(0x44E35000 + 0x28);

	PUT32(0x44E35000 + 0x28, SYN_WDT_LOAD_VALUE);

	cout << "WDT_LOAD_VALUE: "<<WDT_LOAD_VALUE;
	printf("\r\n");

	cout << "WDT STATUS    : "<<WDT_FUCKING;
	printf("\r\n");

	NVIC_ClearPending(RTCINT_IRQn);		// Clear RTC int
	int time_second = GET32(0x44E3E000 + 0x00);		/*get seconds from RTC */
	int time_minute = GET32(0x44E3E000 + 0x04);		/*get hours from RTC */
	int time_hour   = GET32(0x44E3E000 + 0x08);		/*get hours from RTC */
	printf("RTC-> ");
	cout << ((time_hour >> 4) & 0b0011) << (time_hour & 0xf);	/* print time_hours */
	printf(":");
	cout << (time_minute >> 4) << (time_minute & 0xf);	/* print seconds*/
	printf(":");
	cout << (time_second >> 4) << (time_second & 0xf) << endl;	/* print seconds*/
	printf("\r\n");

	count++;

	cout << "stamp_count->["<<count<<"]"<<endl;
	printf("\r\n");
}

class test_class
{
public:
	test_class()
	{
		printf("test_class: ctor\r\n");
	}
	void test()
	{
		printf("test_class: test\r\n");
	}
	~test_class()
	{
		printf("test_class: dtor\r\n");
	}
};

vector <int> *l;

int main () {

	l = new vector <int>;

	l->push_back(0b0000);
	l->push_back(0b0001);
	l->push_back(0b0011);

	l->push_back(0b0010);
	l->push_back(0b0110);
	l->push_back(0b0111);
	l->push_back(0b0101);
	l->push_back(0b0100);
	l->push_back(0b1100);
	l->push_back(0b1101);
	l->push_back(0b1111);
	l->push_back(0b1110);
	l->push_back(0b1010);
	l->push_back(0b1011);
	l->push_back(0b1001);
	l->push_back(0b1000);

	CKM_setCLKModuleRegister(CKM_RTC,CKM_RTC_CLKSTCTRL, 0x2);     // software wakeup on RTC power domain
	CKM_setCLKModuleRegister(CKM_RTC,CKM_RTC_RTC_CLKCTRL, 0x2);   // enable RTC clock power domain

	PUT32(0x44E3E000 + 0x6C, 0x83E70B13);     /* disable protection on register */
	PUT32(0x44E3E000 + 0x70, 0x95A4F1E0);     /* disable protection on register */

	PUT32(0x44E3E000 + 0x40, 0x1);			/* run RTC */

	PUT32(0x44E3E000 + 0x54, 1<<3 | 1<<6);	/* enable 32khz (bit 6) & select 32 khz osc */

	PUT32((0x44E3E000 + 0x04), 0x25);
	PUT32((0x44E3E000 + 0x08), 0x08);

	while(GET32(0x44E3E000 + 0x44) & 0x1);	/* wait until RTC is done updating */

	PUT32(0x44E3E000 + 0x48, 0x4);			/* enable interrupt, every second */

	NVIC_Enable(RTCINT_IRQn);

	SYN_WDT_LOAD_VALUE = GET32(0x44E35000 + 0x28);




	printf("[HEX]__data_start__ = 0x%8X\r\n",(int)& __data_start__);
	printf("[HEX]__data_end__   = 0x%8X\r\n",(int)& __data_end__);

	while(1) {
		test_class t;
		t.test();
		for(uint32_t i = 0; i < l->size(); i++) {
			asm volatile ("wfi");
			cout << "UART_RX"<<uart0_rbuf<<endl;
			printf("\r\n");
			LED_setValue(l->at(i));
		}
	}

}

//setenv ipaddr 192.168.168.133;setenv serverip 192.168.168.144;setenv fdtaddr 0x88000000;setenv loadaddr 0x82000000;setenv loadfdt 'tftpboot ${fdtaddr} am335x-boneblack.dtb';setenv loadimage 'tftpboot ${loadaddr} zImage';setenv bootcmd 'run loadimage; run loadfdt; bootz ${loadaddr} - ${fdtaddr}'


